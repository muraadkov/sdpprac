package glhf;

public interface AbstractFactory<T> {
    Car buildCar();
}
