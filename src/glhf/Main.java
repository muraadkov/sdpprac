package glhf;

import java.awt.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Beginning of creating car...");
        System.out.println("What car do you want: \nKIA\nSKODA\nVOLKSWAGEN");
        String carName = sc.nextLine();
        if(carName.toUpperCase().equals("KIA")){
            System.out.println("What model do you want");

            String carModel = sc.nextLine();

            System.out.println("Let`s go to characteristics");
            System.out.println("What color do you want");
            String carColor = sc.nextLine();

            System.out.println("What engine volume do you want?");
            double engineVolume = sc.nextDouble();
            Car kia = CarFactory.buildCar(new KiaFactory());
            kia.setModel(carModel);
            kia.setColor(CarColor.valueOf(carColor));
            kia.setEngineVolume(engineVolume);
        }else if(carName.toUpperCase().equals("SKODA")){
            System.out.println("What model do you want");

            String carModel = sc.nextLine();

            System.out.println("Let`s go to characteristics");
            System.out.println("What color do you want");
            String carColor = sc.nextLine();

            System.out.println("What engine volume do you want?");
            double engineVolume = sc.nextDouble();
            Car skoda = CarFactory.buildCar(new SkodaFactory());
            skoda.setModel(carModel);
            skoda.setColor(CarColor.valueOf(carColor));
            skoda.setEngineVolume(engineVolume);
        }else if(carName.toUpperCase().equals("VOLKSWAGEN")){
            System.out.println("What model do you want");

            String carModel = sc.nextLine();

            System.out.println("Let`s go to characteristics");
            System.out.println("What color do you want");
            String carColor = sc.nextLine();

            System.out.println("What engine volume do you want?");
            double engineVolume = sc.nextDouble();
            Car volkswagen = CarFactory.buildCar(new VolkswagenFactory());
            volkswagen.setModel(carModel);
            volkswagen.setColor(CarColor.valueOf(carColor));
            volkswagen.setEngineVolume(engineVolume);
        }


    }
}
