package glhf;

public class Car {

    String model;
    double engineVolume;
    CarColor color;
    Car(){

    }

    Car(String model, double engineVolume, CarColor color){
      setModel(model);
      setEngineVolume(engineVolume);
      setColor(color);
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public double getEngineVolume() {return engineVolume;}

    public CarColor getColor() {return color;}


    public void setEngineVolume(double engineVolume){ this.engineVolume = engineVolume;}

    public void setColor(CarColor color) {this.color = color;}
}
