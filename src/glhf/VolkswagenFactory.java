package glhf;

public class VolkswagenFactory implements AbstractFactory {

    private String model;
    private CarColor color;
    private double engineVolume;
    @Override
    public Car buildCar() {
        return new Volkswagen(model, engineVolume, color);
    }

}
